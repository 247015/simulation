package simulation.processor;

import simulation.taskGenerator.Task;

public class CPU {
    Task executedTask;

    CPU(){
        executedTask = null;
    }

    public Task getExecutedTask() {
        return executedTask;
    }

    public void setExecutedTask(Task executedTask) {
        this.executedTask = executedTask;
    }

    public boolean isBusy(){
        if(executedTask==null){
            return false;
        }
        return true;
    }

    private void iterate(){
    
    }


}
