package simulation.processor;

import java.util.ArrayList;
import java.util.LinkedList;

public class CPUGroup {

    private ArrayList<CPU> cpuList;
    private int size;

    CPUGroup(int size){
        size = size;
    }

    private ArrayList<CPU> create(){
        for(int i=0; i<size; i++){
            cpuList.add(new CPU());
        }
        return cpuList;
    }

    public ArrayList<CPU> getCpuList() {
        return cpuList;
    }

    public void setCpuList(ArrayList<CPU> cpuList) {
        this.cpuList = cpuList;
    }

    public int getSize() {
        return size;
    }

    public Integer[] getFreeCPUs(){
        LinkedList<Integer> indexes = new LinkedList<>();
        for(int i=0;i<size;i++){
            if(!(cpuList.get(i).isBusy())){
                indexes.add(i);
            }
        }
        return (Integer[]) indexes.toArray();
    }
}
