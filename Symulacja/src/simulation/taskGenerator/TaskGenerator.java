package simulation.taskGenerator;

import java.util.ArrayList;
import java.util.Random;

public class TaskGenerator {
	 
    //Clock clock;
 
    public TaskGenerator() {
 
    }
 
    public ArrayList<Task> generate(int timeLimit, int numberLimit, int priorityLimit) {
        Random rand = new Random();
        ArrayList<Task> list = new ArrayList<>(numberLimit);
 
        int tasksNumber = rand.nextInt(numberLimit);
        for (int i = 0 ; i < tasksNumber ; i++) {
            list.add(new Task(rand.nextInt(timeLimit) + 1, rand.nextInt(priorityLimit)));
        }
 
        return list;
    }
}