package simulation.taskGenerator;

public class Task {
	 
    private int requiredTime;
    private int priority;
    private int waitingTime;
 
    public Task(int requiredTime, int priority) {
        this.requiredTime = requiredTime;
        this.priority = priority;
    }
 
    public int getRequiredTime() {
        return requiredTime;
    }
 
    public void setRequiredTime(int requiredTime) {
        this.requiredTime = requiredTime;
    }
 
    public int getPriority() {
        return priority;
    }
 
    public void setPriority(int priority) {
        this.priority = priority;
    }
 
    @Override
    public String toString() {
        return "Task{" +
                "requiredTime=" + requiredTime +
                ", priority=" + priority +
                '}';
    }
}