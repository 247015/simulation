package simulation.manager;
import simulation.schedulingAlgorithm.SchedulingAlgorithm;
import simulation.taskGenerator.Task;

import java.util.ArrayList;

public class TaskManager implements Tick{
    
    private Clock clock = new Clock();
    private Thread thread = new Thread(clock);
    private ArrayList<Task> queue = new ArrayList<>();
    private SchedulingAlgorithm algorithm;
    private long sum = 0;
    
    
    public TaskManager(SchedulingAlgorithm algorithm){
        this.algorithm = algorithm;
        clock.addObserver(this);
        thread.start();
    }
    
    public ArrayList<Task> getTaskList(int n){
        ArrayList<Task> temp = algorithm.findNext(queue,n);
        return temp;
    }
    
    @Override
    public void tick (long currentTick) {
        System.out.println("Manager tick: "+currentTick);
    }
}