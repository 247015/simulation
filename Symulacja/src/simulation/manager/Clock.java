package simulation.manager;

import simulation.manager.Tick;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Clock implements Runnable {

    private ArrayList<Tick> tickObservers = new ArrayList<>();
    private long currentTick = 0;
    private boolean continueLoop = true;
    
    @Override
    public void run () {
        updateTick();
    }
    
    public void updateTick(){
        while(continueLoop){
            /*try{
                TimeUnit.MILLISECONDS.sleep(100);
                notifyAll(currentTick++);
            }catch (InterruptedException e){
                System.out.println("Time sleep interruption");
            }*/
            notifyAll(currentTick++);
        }
    }
    
    private void notifyAll(long tick){
        for(Tick t: tickObservers){
            t.tick(tick);
        }
    }
    
    public void stop(){
        continueLoop = false;
    }
    
    public void addObserver(Tick t){
        tickObservers.add(t);
    }
    
}
