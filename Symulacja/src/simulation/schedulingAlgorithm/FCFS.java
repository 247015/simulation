package simulation.schedulingAlgorithm;

import simulation.taskGenerator.Task;

import java.util.ArrayList;

public class FCFS implements SchedulingAlgorithm {
    
    @Override
    public ArrayList<Task> findNext (ArrayList<Task> list, int n) {
        ArrayList<Task> temp = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            temp.add(list.remove(0));
        return temp;
    }
}
