

import java.io.*;


public class Presenter {


    private int liczba_procesorow;

    public void data(String name, double time) {
        System.out.println("Czas średni dla algorytmu  "+name+": "+time);
    }

    public void start() throws IOException {
        System.out.println("Ile będzie procesów?");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        liczba_procesorow = Integer.parseInt(reader.readLine());
    }

    public int getLiczba_procesorow() {
        return liczba_procesorow;
    }

    public void setLiczba_procesorow(int liczba_procesorow) {
        this.liczba_procesorow = liczba_procesorow;
    }


    public void SaveData(String name, int time) throws FileNotFoundException {
        Writer output = null;
        try {
            output = new BufferedWriter(new FileWriter("text.txt", true));
            output.append("Sredni czas dla " + name + " :" + time + "\n");
            output.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}


